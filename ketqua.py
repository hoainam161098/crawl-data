
from bs4 import BeautifulSoup
import urllib.request
#asdasdasdasd

def main():
   

    req = urllib.request.Request(
        url="https://ketqua.net",
        headers={
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0" 
        },
    )
    with urllib.request.urlopen(req) as f:
        content = f.read().decode()
    content = BeautifulSoup(content, 'lxml')
    
    day = content.find("span", {"id": "result_date"}).text.rjust(45)
    

    giai_dac_biet = content.find("div", {"id": "rs_0_0"}).text.rjust(25)

    giai_nhat     = content.find("div", {"id": "rs_1_0"}).text.rjust(30)

    giai_nhi_1     = content.find("div", {"id": "rs_2_0"}).text.rjust(28)
    giai_nhi_2     = content.find("div", {"id": "rs_2_1"}).text

    giai_ba_1      = content.find("div", {"id": "rs_3_0"}).text.rjust(25)
    giai_ba_2      = content.find("div", {"id": "rs_3_1"}).text
    giai_ba_3      = content.find("div", {"id": "rs_3_2"}).text
    giai_ba_4      = content.find("div", {"id": "rs_3_3"}).text.rjust(35)
    giai_ba_5      = content.find("div", {"id": "rs_3_4"}).text
    giai_ba_6      = content.find("div", {"id": "rs_3_5"}).text
    
    giai_tu_1      = content.find("div", {"id": "rs_4_0"}).text.rjust(23)
    giai_tu_2      = content.find("div", {"id": "rs_4_1"}).text
    giai_tu_3      = content.find("div", {"id": "rs_4_2"}).text
    giai_tu_4      = content.find("div", {"id": "rs_4_3"}).text
    
    giai_nam_1     = content.find("div", {"id": "rs_5_0"}).text.rjust(23)
    giai_nam_2     = content.find("div", {"id": "rs_5_1"}).text
    giai_nam_3     = content.find("div", {"id": "rs_5_2"}).text
    giai_nam_4     = content.find("div", {"id": "rs_5_3"}).text.rjust(35)
    giai_nam_5     = content.find("div", {"id": "rs_5_4"}).text
    giai_nam_6     = content.find("div", {"id": "rs_5_5"}).text
    

    giai_sau_1     = content.find("div", {"id": "rs_6_0"}).text.rjust(25)
    giai_sau_2     = content.find("div", {"id": "rs_6_1"}).text
    giai_sau_3     = content.find("div", {"id": "rs_6_2"}).text

    giai_bay_1     = content.find("div", {"id": "rs_7_0"}).text.rjust(25)
    giai_bay_2     = content.find("div", {"id": "rs_7_1"}).text
    giai_bay_3     = content.find("div", {"id": "rs_7_2"}).text
    giai_bay_4     = content.find("div", {"id": "rs_7_3"}).text

    print("XỔ SỐ TRUYỀN THỐNG".rjust(50))
    print("Ngày: ",day)
    print("Giải Đặc Biệt: ", giai_dac_biet)
    print("Giải Nhất: ", giai_nhat)
    print("Giải Nhì: ", giai_nhi_1, giai_nhi_2)
    print("Giải Ba: ", giai_ba_1, giai_ba_2, giai_ba_3)
    print(giai_ba_4, giai_ba_5, giai_ba_6)
    print("Giải Tư: ", giai_tu_1, giai_tu_2, giai_tu_3, giai_tu_4)
    print("Giải Năm: ", giai_nam_1, giai_nam_2, giai_nam_3)
    print(giai_nam_4, giai_nam_5, giai_nam_6)
    print("Giải Sáu: ", giai_sau_1, giai_sau_2, giai_sau_3)
    print("Giải Bảy", giai_bay_1, giai_bay_2, giai_bay_3, giai_bay_4)




if __name__ == "__main__":
    main()
